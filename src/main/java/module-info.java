module cz.cvut.fel.pjv.kopcaboh {
    requires javafx.controls;
    requires javafx.fxml;

    opens cz.cvut.fel.pjv.kopcaboh to javafx.fxml;
    exports cz.cvut.fel.pjv.kopcaboh;
}