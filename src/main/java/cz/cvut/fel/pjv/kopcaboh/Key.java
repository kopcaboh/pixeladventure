package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

public class Key extends Item {
    private final int keyID;

    public Key(int value, String name, Image sprite, int keyID) {
        super(value, name, sprite);
        this.keyID = keyID;
    }

    @Override
    public void launchClickAction() {

    }

    @Override
    public Image draw() {
        return null;
    }
}
