package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

import java.io.Serializable;

public class Chest implements Drawable, Clickable, Lootable, Serializable {
    private final int chestID;
    private Image sprite;

    public Chest(int chestID, Image sprite) {
        this.chestID = chestID;
        this.sprite = sprite;
    }

    @Override
    public void launchClickAction() {

    }

    @Override
    public Image draw() {
        return null;
    }

    @Override
    public Item dropItem() {
        return null;
    }
}
