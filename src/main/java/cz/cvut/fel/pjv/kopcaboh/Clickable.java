package cz.cvut.fel.pjv.kopcaboh;

public interface Clickable {
    /**
     * Must be implemented by all objects that react on click
     */
    public void launchClickAction();
}
