package cz.cvut.fel.pjv.kopcaboh;

public interface Lootable {

    /**
     * Makes object drop an item
     * Use for chests, completed quests or enemy kill drops
     */
    public Item dropItem();
}
