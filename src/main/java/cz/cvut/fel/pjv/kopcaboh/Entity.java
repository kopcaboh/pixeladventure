package cz.cvut.fel.pjv.kopcaboh;

import java.io.Serializable;

abstract public class Entity implements Clickable, Drawable, Serializable {
    private int health;
    private int penetration;
    private int damage;
    private int defense;

    public Entity(int health, int penetration, int damage, int defense) {
        this.health = health;
        this.penetration = penetration;
        this.damage = damage;
        this.defense = defense;
    }

    /**
     * Recieve hit dealt by a weapon or hand.
     * Damage can be handled by the recieving object (blocked, etc.)
     * @param penetration
     * @param damage
     */
    public abstract void recieveHit(int penetration, int damage);

    /**
     * Recieve direct damage, eg. from a trap or a spell.
     * @param damage
     */
    public abstract void recieveDamage(int damage);

    /**
     * Make object die
     */
    public abstract void die();

}
