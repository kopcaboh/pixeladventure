package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

import java.io.Serializable;

public abstract class Item implements Clickable, Drawable, Serializable {
    private int value;
    private final String name;
    private Image sprite;

    public Item(int value, String name, Image sprite) {
        this.value = value;
        this.name = name;
        this.sprite = sprite;
    }

    /**
     * Returns in-game market value
     * @return market value
     */
    public int getValue() {
        return value;
    }

    /**
     * Returns in-game name of an object
     * @return name
     */
    public String getName() {
        return name;
    }
}
