package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

import java.io.Serializable;

public class Door implements Drawable, Clickable, Serializable {
    private final int doorID;
    private Image sprite;

    public Door(int doorID, Image sprite) {
        this.doorID = doorID;
        this.sprite = sprite;
    }

    @Override
    public void launchClickAction() {

    }

    @Override
    public Image draw() {
        return null;
    }
}
