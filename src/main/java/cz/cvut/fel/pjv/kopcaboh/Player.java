package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

public class Player extends Entity{
    private Inventory inventory;

    public Player(int health, int penetration, int damage, int defense) {
        super(health, penetration, damage, defense);
        inventory = new Inventory();
    }

    /**
     * Adds item to inventory
     * @param item
     */
    public void getItem(Item item) {
        inventory.add(item);
    }

    /**
     * Puts item on ground
     * @param item
     */
    public void throwOutItem(Item item) {
        inventory.remove(item);
    }

    /**
     * Puts item into 'equipped' mode and changes player stats
     * @param item
     */
    public void equipItem(Item item) {
        inventory.equip(item);
        // Change stats
    }

    @Override
    public void recieveHit(int penetration, int damage) {

    }

    @Override
    public void recieveDamage(int damage) {

    }

    @Override
    public void die() {

    }

    @Override
    public void launchClickAction() {

    }

    @Override
    public Image draw() {
        return null;
    }
}
