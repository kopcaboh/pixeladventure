package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

abstract public class EquippableItem extends Item {
    boolean equipped = false;

    public EquippableItem(int value, String name, Image sprite) {
        super(value, name, sprite);
    }

    /**
     * Unequips item.
     * Can be used by game events - monster can unequip your weapon, etc.
     */
    public abstract void unequip();

    /**
     * Internally handles equipping
     */
    public abstract void equip();
}