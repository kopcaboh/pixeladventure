package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

public class Enemy extends Entity implements Lootable {
    public Enemy(int health, int penetration, int damage, int defense) {
        super(health, penetration, damage, defense);
    }

    @Override
    public void recieveHit(int penetration, int damage) {

    }

    @Override
    public void recieveDamage(int damage) {

    }

    @Override
    public void die() {

    }

    @Override
    public void launchClickAction() {

    }

    @Override
    public Image draw() {
        return null;
    }

    @Override
    public Item dropItem() {
        return null;
    }
}
