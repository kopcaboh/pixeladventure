package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

public class Weapon extends EquippableItem {
    private int penetration;
    private int damage;

    public Weapon(int value, String name, Image sprite, int penetration, int damage) {
        super(value, name, sprite);
        this.penetration = penetration;
        this.damage = damage;
    }

    @Override
    public void unequip() {

    }

    @Override
    public void equip() {

    }

    @Override
    public void launchClickAction() {

    }

    @Override
    public Image draw() {
        return null;
    }
}
