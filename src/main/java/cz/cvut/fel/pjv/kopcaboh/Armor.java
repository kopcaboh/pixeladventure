package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

public class Armor extends EquippableItem {
    private int defense;

    public Armor(int value, String name, Image sprite, int defense) {
        super(value, name, sprite);
        this.defense = defense;
    }

    @Override
    public void unequip() {

    }

    @Override
    public void equip() {

    }

    @Override
    public void launchClickAction() {

    }

    @Override
    public Image draw() {
        return null;
    }
}
