package cz.cvut.fel.pjv.kopcaboh;

import javafx.scene.image.Image;

public interface Drawable {

    /**
     * This method will be used by controllers to redraw game sprites
     */
    public Image draw();
}
